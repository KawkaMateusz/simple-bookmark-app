// Try generate existing bookmarks
const body = document.querySelector('body');
body.addEventListener('onload', generateBookmarks());

// listen for form submit
document.querySelector('#myForm').addEventListener('submit', saveBookmark);

// save bookmark
function saveBookmark() {
    // get form values
    let siteName = document.querySelector('#siteName').value;
    let siteUrl = document.querySelector('#siteUrl').value;

    let bookmark = {
        name: siteName,
        url: siteUrl
    }

    // test if bookmarks is null
    if (localStorage.getItem('bookmarks') === null) {
        
        // Init array
        let bookmarks = [];
        
        // Add to array
        bookmarks.push(bookmark)
        
        //Set to local storage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    } else {
        // Get bookmarks from local storage
        let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
        
        // Add bookmark to array
        bookmarks.push(bookmark);
        
        //Re-set back to localStorage;
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    }

    //Re generate bookmarks

    generateBookmarks();
}

//Delete bookmark 

function deleteBookmark(url) {
    //get bookmarks form local storage
    let bookmarks =
        JSON.parse(localStorage.getItem('bookmarks'));
    for (let i = 0; i < bookmarks.length; i++) {
        if (bookmarks[i].url == url) {
            //remove from array
            bookmarks.splice(i, 1);
        }
    }
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

    //Re generate bookmarks

    generateBookmarks();
}


//generate bookmarks

function generateBookmarks() {
    // Get bookmarks from local storage
    let bookmarks =
        JSON.parse(localStorage.getItem('bookmarks'));
    
    // test if bookmarks is null
    if (bookmarks === null) {
        
        // Init array
        bookmarks = [];
    }
    // Get ouput id

    let bookmarksResults = document.querySelector('#bookmarksResults');

    //Build ouput

    bookmarksResults.innerHTML = '';
    for (let i = 0; i < bookmarks.length; i++) {
        let name = bookmarks[i].name;
        let url = bookmarks[i].url;

        bookmarksResults.innerHTML += '<div>' +
            '<p class="lead">' + name +
            ' <a class="btn btn-primary btn-sm" target="_blank" href="' + url + '">Visit</a>' +
            ' <a onclick="deleteBookmark(\'' + url + '\')" class="btn btn-danger btn-sm" href="#">Delete</a>' +
            '</p></div>';
    }
}